FROM openjdk:8-jre-alpine

# expose the port 8080 for connections
EXPOSE 8080

# copy .jar file to given directory
COPY ./target/spring-petclinic-*.jar /usr/app/
# change directory so the .jar file can be run using CMD
WORKDIR /usr/app

CMD java -jar spring-petclinic-*.jar
